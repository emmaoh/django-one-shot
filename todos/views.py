from audioop import reverse
from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from todos.models import TodoItem, TodoList
from django.urls import reverse_lazy

class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"

def get_context_data(self, **kwargs):
    context = super().get_context_data(**kwargs)
    from pprint import pprint
    pprint(context)
    return context
  
class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]
    success_url = reverse_lazy("todo_list")

    def form_valid(self, form):
        return super().form_valid(form)

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    success_url = reverse_lazy("todo_list")

    def form_valid(self, form):
        return super().form_valid(form)

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list")

class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/items/create.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todo_list")

    def form_valid(self, form):
        return super().form_valid(form)
    
    #def get_success_url(self) -> str:
        #return reverse_lazy("todo_detail", args=[self.object.id])

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/items/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todo_list")

    #def get_success_url(self) -> str:
        #return reverse_lazy()

    
    
    #   def get_success_url(self):
        #todolistid=self.kwargs['pk']
        #return reverse_lazy ("todo_detil", kwargs={'pk': todolistid})

    def form_valid(self, form):
        return super().form_valid(form)





